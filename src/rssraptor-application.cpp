#include "rssraptor-application.h"

namespace RSSRaptor {

Application::Application()
    : _window()
{
    _app = Gtk::Application::create("me.jsfer.rssraptor");
}

Application::~Application()
{

}

int Application::start()
{
    return _app->run(_window);
}

}
