#pragma once

#include <gtkmm-3.0/gtkmm.h>

namespace RSSRaptor {

class Window : public Gtk::ApplicationWindow {
public:
    Window();
    ~Window();

private:
};

}
