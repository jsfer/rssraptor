#pragma once

#include "rssraptor-window.h"

#include <gtkmm-3.0/gtkmm.h>
#include <glibmm/refptr.h>

namespace RSSRaptor {

class Application : public Gtk::Application {
public:
    Application();
    ~Application();

    int start();

private:
    Glib::RefPtr<Gtk::Application> _app;
    RSSRaptor::Window _window;
};

}
