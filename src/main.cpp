#include "rssraptor.h"
#include "rssraptor-window.h"
#include "rssraptor-application.h"
#include <iostream>

int main() {
    std::cout << RSSRAPTOR_RESOURCE_DIR << "\n";
    auto app = RSSRaptor::Application();
    return app.start();
}
